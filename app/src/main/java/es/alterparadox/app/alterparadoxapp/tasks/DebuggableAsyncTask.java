package es.alterparadox.app.alterparadoxapp.tasks;

import android.os.AsyncTask;

abstract class DebuggableAsyncTask<Params, Progress, Result> extends AsyncTask<Params, Progress, Result> {
    @SuppressWarnings({"unchecked", "varargs"})
    protected abstract Result doBackgroundTask(Params... params);

    @Override
    @SafeVarargs
    protected final Result doInBackground(Params... params) {
        if (android.os.Debug.isDebuggerConnected()) {
            android.os.Debug.waitForDebugger();
        }

        return doBackgroundTask(params);
    }
}
