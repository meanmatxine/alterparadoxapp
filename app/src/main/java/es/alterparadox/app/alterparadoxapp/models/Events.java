package es.alterparadox.app.alterparadoxapp.models;

import android.util.Log;

import net.fortuna.ical4j.model.DateTime;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Comparator;

import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

/**
 * Created by hetta on 06/02/2018.
 */

public class Events implements Serializable {
    private String Name;
    private String DateStart;
    private String Location;
    private String Type;

    public Events(String Name, String DateStart, String Rule, String Location, String Type) {
        this.Name=Name.replace("\n","").replace("\r","").replace("SUMMARY:","");

        String thedate = DateStart.replace("\n","").replace("\r","").replace("DTSTART:","").replace("DTSTART;VALUE=DATE:","");
        if(thedate.equals("")){
            thedate = "20180101T010000Z";
        }

        if(thedate.contains("Z")){
            try{
                DateTime from = new DateTime(thedate);
                SimpleDateFormat sdfDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");//dd/MM/yyyy
                this.DateStart = sdfDate.format(from);
            }catch (Exception e){
                Log.e("Error",e.getMessage());
            }
        }else{
            try{
                //Date from = new Date(thedate.substring(0,4)+"-"+thedate.substring(4,6)+"-"+thedate.substring(6,8));
                DateTime from = new DateTime(thedate+"T010000Z");
                Calendar cal = Calendar.getInstance();
                cal.setTime(from);
                if(Rule.contains("FREQ=YEARLY")){
                    cal.set(Calendar.YEAR, Calendar.getInstance().get(Calendar.YEAR)); // Where n is int
                }
                SimpleDateFormat sdfDate = new SimpleDateFormat("yyyy-MM-dd");//dd/MM/yyyy
                this.DateStart = sdfDate.format(cal.getTime());
            }catch (Exception e){
                Log.e("Error",e.getMessage());
            }
        }

        this.Location=Location.replace("\n","").replace("\r","").replace("LOCATION:","");


        this.Type=Type;
    }
    public String getName() {
        return this.Name;
    }
    public void setName(String Value) {
        this.Name=Name;
    }
    public String getDateStart() {
        return this.DateStart;
    }
    public void setDateStart(String DateStart) {
        this.DateStart=DateStart;
    }
    public String getLocation() {
        return this.Location;
    }
    public void setLocation(String Location) {
        this.Location=Location;
    }
    public String getType() {
        return this.Type;
    }
    public void setType(String Type) {
        this.Type=Type;
    }

}
