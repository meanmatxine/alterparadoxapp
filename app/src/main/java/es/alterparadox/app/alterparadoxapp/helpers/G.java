package es.alterparadox.app.alterparadoxapp.helpers;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;

/**
 * Created by hetta on 08/01/2018.
 */

public final class G {
    public static String Email="";
    public static String Name="";
    public static int Member=0;
    public static String LastDate="2017-12-31 00:00:00";

    public static final String URLChecks = "http://alterparadox.es/checkapp.php";
    public static final String URLLinks = "http://alterparadox.es/links.php";
    public static final String URLCalendar = "https://calendar.google.com/calendar?cid=YWx0ZXJwYXJhZG94QGdtYWlsLmNvbQ";

    public static GoogleSignInClient mGoogleSignInClient;

    public static void init(Context context){
        SharedPreferences sharedPref = context.getSharedPreferences("Settings", Context.MODE_PRIVATE);
        Member = sharedPref.getInt("Member", 0);
        Name = sharedPref.getString("Name", "");
        Email = sharedPref.getString("Email", "");
        LastDate = sharedPref.getString("LastDate", "2017-12-31 00:00:00");
    }
    public static String getName(Context context){
        SharedPreferences sharedPref = context.getSharedPreferences("Settings", Context.MODE_PRIVATE);
        return sharedPref.getString("Name", "");
    }
    public static String getEmail(Context context){
        SharedPreferences sharedPref = context.getSharedPreferences("Settings", Context.MODE_PRIVATE);
        return sharedPref.getString("Email", "");
    }
    public static String getLastDate(Context context){
        SharedPreferences sharedPref = context.getSharedPreferences("Settings", Context.MODE_PRIVATE);
        return sharedPref.getString("LastDate", "2017-12-31 00:00:00");
    }
    public static int getMember(Context context){
        SharedPreferences sharedPref = context.getSharedPreferences("Settings", Context.MODE_PRIVATE);
        return sharedPref.getInt("Member", 0);
    }

    public static void setName(Context context, String name){
        Name = name;
        SharedPreferences sharedPref = context.getSharedPreferences("Settings", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString("Name", name);
        editor.commit();
    }

    public static void setEmail(Context context, String email){
        Email = email;
        SharedPreferences sharedPref = context.getSharedPreferences("Settings", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString("Email", email);
        editor.commit();
    }
    public static void setLastDate(Context context, String lastdate){
        LastDate = lastdate;
        SharedPreferences sharedPref = context.getSharedPreferences("Settings", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString("LastDate", lastdate);
        editor.commit();
    }

    public static void setMember(Context context, int member){
        Member = member;
        SharedPreferences sharedPref = context.getSharedPreferences("Settings", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putInt("Member", member);
        editor.commit();
    }

    public static void updateUI(Context context,GoogleSignInAccount account){
        if(account == null){
            Log.i("GOOGLESIGN","NULL");
            G.Email = "";
            G.Name = "";
            setName(context, G.Name);
            setEmail(context,G.Email);
            return;
        }
        if(account.getEmail().equals("")){
            G.Email = "";
            G.Name = "";
            Log.i("GOOGLESIGN","TODO MAL");
            setName(context, G.Name);
            setEmail(context,G.Email);
            return;
        }else{
            //PArty
            Log.i("GOOGLESIGN",account.getEmail());
            //Toast.makeText(getApplicationContext(), "Registro correcto. Usuario:" + account.getDisplayName() + " Email:"+ account.getEmail() , Toast.LENGTH_SHORT).show();
            G.Email = account.getEmail();
            G.Name = account.getDisplayName();
        }
        setName(context, G.Name);
        setEmail(context,G.Email);
    }


}
