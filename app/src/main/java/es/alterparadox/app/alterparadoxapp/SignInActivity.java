package es.alterparadox.app.alterparadoxapp;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;

import es.alterparadox.app.alterparadoxapp.helpers.G;

/**
 * Created by hetta on 07/01/2018.
 */

public class SignInActivity extends AppCompatActivity {

    Context context;
    private int RC_SIGN_IN=1;
    LinearLayout LSignIn;
    LinearLayout LSignOut;
    TextView txtName;
    TextView txtEmail;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.account);
        context = this;
        LSignIn = findViewById(R.id.LSignIn);
        LSignOut = findViewById(R.id.LSignOut);
        txtName = findViewById(R.id.txtName);
        txtEmail = findViewById(R.id.txtEmail);
        updateUI();

        // Set the dimensions of the sign-in button.
        SignInButton signInButton = findViewById(R.id.sign_in_button);
        signInButton.setSize(SignInButton.SIZE_STANDARD);
        signInButton.setOnClickListener( new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                signIn();
            }
        });
        // Set the dimensions of the sign-in button.
        Button signoutbutton = findViewById(R.id.sign_out_button);

        signoutbutton.setOnClickListener( new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                signOut();
            }
        });
    }

    public void signIn(){
        Intent signInIntent = G.mGoogleSignInClient.getSignInIntent();
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    public void signOut(){

        G.mGoogleSignInClient.signOut()
                .addOnCompleteListener(this, new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        // ...
                        G.updateUI(context, null);
                        updateUI();
                    }
                });
    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // Result returned from launching the Intent from GoogleSignInClient.getSignInIntent(...);
        if (requestCode == RC_SIGN_IN) {
            // The Task returned from this call is always completed, no need to attach
            // a listener.
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            handleSignInResult(task);

            //GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            //int statusCode = result.getStatus().getStatusCode();
        }
    }
    private void handleSignInResult(Task<GoogleSignInAccount> completedTask) {
        try {
            GoogleSignInAccount account = completedTask.getResult(ApiException.class);

            // Signed in successfully, show authenticated UI.
            G.updateUI(context, account);
            updateUI();
            //return;
        } catch (ApiException e) {
            // The ApiException status code indicates the detailed failure reason.
            // Please refer to the GoogleSignInStatusCodes class reference for more information.
            Log.w("GOOGLESIGN", "signInResult:failed code=" + e.getStatusCode() + " " + e.getStatusMessage());
            G.updateUI(context,null);
            updateUI();
        }
    }
    private void updateUI(){
        if(G.Email.equals("")){
            LSignIn.setVisibility(View.VISIBLE);
            LSignOut.setVisibility(View.GONE);
        }else{
            LSignIn.setVisibility(View.GONE);
            LSignOut.setVisibility(View.VISIBLE);
            txtName.setText(G.Name);
            txtEmail.setText(G.Email);
        }
    }


}
