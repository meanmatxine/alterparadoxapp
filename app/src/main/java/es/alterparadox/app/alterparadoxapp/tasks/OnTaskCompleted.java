package es.alterparadox.app.alterparadoxapp.tasks;

public interface OnTaskCompleted {
    void onTaskCompleted(Object result);
}