package es.alterparadox.app.alterparadoxapp;

import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import net.fortuna.ical4j.data.CalendarBuilder;
import net.fortuna.ical4j.data.ParserException;
import net.fortuna.ical4j.filter.Filter;
import net.fortuna.ical4j.filter.PeriodRule;
import net.fortuna.ical4j.model.Component;
import net.fortuna.ical4j.model.DateTime;
import net.fortuna.ical4j.model.Dur;
import net.fortuna.ical4j.model.Period;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;

import es.alterparadox.app.alterparadoxapp.adapters.EventsAdapter;
import es.alterparadox.app.alterparadoxapp.helpers.G;
import es.alterparadox.app.alterparadoxapp.models.Events;
import es.alterparadox.app.alterparadoxapp.models.Items;
import es.alterparadox.app.alterparadoxapp.tasks.OnTaskCompleted;
import es.alterparadox.app.alterparadoxapp.tasks.getURLFile;
import android.net.Uri;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import es.alterparadox.app.alterparadoxapp.adapters.ItemsAdapter;
import es.alterparadox.app.alterparadoxapp.tasks.getURLPostJSON;

import java.util.Collections;
import java.util.Comparator;


public class Fragment1 extends Fragment {
    private static JSONArray JArray;
    private static ListView listView;
    public Fragment1() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_main, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        // Setup any handles to view objects here
        // EditText etFoo = (EditText) view.findViewById(R.id.etFoo);

        Bundle bundle = getArguments();
        String mDocNum = bundle.getString("load");
        Log.e("bundle?",mDocNum);

        listView = (ListView)getView().findViewById(R.id.lstView);
        switch(mDocNum){
            case "links":
                loadLinks();
                break;
            case "doodle":
                loadDoodles();
                break;
            case "notice":
                loadNotices();
                break;
            case "calendar":
                loadEvents();
                break;
        }
        testCalendar();
    }
    private void testCalendar(){

        File filereal = new File(Environment.getExternalStorageDirectory().toString() + "/basic.ics");

        if(!filereal.exists()){
            new getURLFile(new OnTaskCompleted() {
                @Override
                public void onTaskCompleted(Object result) {
                    if (result != null) {
                        if((Boolean)result){
                            File file = new File(Environment.getExternalStorageDirectory().toString() + "/2011.kml");
                            File filereal = new File(Environment.getExternalStorageDirectory().toString() + "/basic.ics");
                            if(file.exists()){
                                if(!filereal.exists()){
                                    file.renameTo(filereal);
                                }else{
                                    if(file.length()!= filereal.length()){
                                        file.renameTo(filereal);
                                    }else{
                                        file.delete();
                                    }
                                }
                            }
                        }
                        readCalendar();
                    }
                }
            }).execute("https://calendar.google.com/calendar/ical/alterparadox@gmail.com/public/basic.ics");
        }
    }

    private void readCalendar(){

        //*Don't* hardcode "/sdcard"
        File sdcard = Environment.getExternalStorageDirectory();

        //Get the text file
        File file = new File(sdcard,"basic.ics");
        // Reading the file and creating the calendar
        CalendarBuilder builder = new CalendarBuilder();
        net.fortuna.ical4j.model.Calendar cal = null;

        try {
            cal = builder.build(new FileInputStream(file));
            Period period = new Period(new DateTime(), new Dur(365, 0, 0, 0));
            Filter filter = new Filter(new PeriodRule(period));
            Collection eventsToday = filter.filter(cal.getComponents(Component.VEVENT));
            ArrayList<Events> lstData = new ArrayList<Events>();

            Events event = new Events("Abrir calendario de Alter Paradox","","","","link");
            lstData.add(event);
            event = new Events("Futuros eventos: ","","","","data");
            lstData.add(event);

            for (Object o : eventsToday) {
                Component c = (Component)o;
                Log.i("Event",c.getProperties("SUMMARY").toString()+c.getProperties("DTSTART").toString()+c.getProperties("LOCATION").toString());
                event = new Events( c.getProperties("SUMMARY").toString(),c.getProperties("DTSTART").toString(),c.getProperties("RRULE").toString(),c.getProperties("LOCATION").toString(),"");
                lstData.add(event);
            }

            for (Events ev: lstData) {
                Log.i("Event",ev.getDateStart());
            }
            //Collections.sort(lstData, Events.StuDateStartComparator);

            Collections.sort(lstData, new Comparator<Events>() {
                @Override
                public int compare(Events o1, Events o2) {
                    return o1.getDateStart().compareTo(o2.getDateStart());
                }}
            );

            listView.setAdapter(new EventsAdapter(getContext(), lstData));

            listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapter, View view, int position, long arg) {
                    Events selectedItem = (Events) listView.getAdapter().getItem(position);
                    if(selectedItem.getType().equals("link")) {
                        loadLink(new Items("links","","link","", G.URLCalendar,0));
                    }
                }
            });


        } catch (IOException e) {
            e.printStackTrace();
        } catch (ParserException e) {
            e.printStackTrace();
        }
    }

    private void loadDoodles(){
        loadURL(G.URLLinks,"doodle");
    }
    private void loadNotices(){
        loadURL(G.URLLinks,"notice");
    }
    private void loadEvents(){
        readCalendar();
    }
    private void loadLinks(){
        loadURL(G.URLLinks, "links");
    }

    public void loadURL(String url, String type){

        new getURLPostJSON(new OnTaskCompleted() {
            @Override
            public void onTaskCompleted(Object result) {
                if (result != null) {
                    loadJson((String) result);
                }
            }
        }).execute(url,type, G.Email, G.Name, G.LastDate);

    }

    public void loadLink(Items link){
        if(link.getType().equals("link")){
            Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(link.getLink()));
            startActivity(browserIntent);
        }
    }

    public void loadJson(String result){

        ArrayList<Items> lstData = new ArrayList<Items>();
        try {

            JSONArray jsonarray = new JSONArray(result);

            if (jsonarray != null) {
                for (int i = 0; i < jsonarray.length(); i++) {
                    JSONObject jsonobject = jsonarray.getJSONObject(i);
                    Items item = new Items(jsonobject.getString("family"),jsonobject.getString("name"), jsonobject.getString("type"), jsonobject.getString("date"), jsonobject.getString("link"), 0);
                    lstData.add(item);
                }
                JArray = jsonarray;
            }
        }catch(JSONException j)
        {
            Log.e( "JSON Exception", "Error :  " + j.getMessage() );
        }
        if(lstData !=null){
            try{
                listView.setAdapter(new ItemsAdapter(getContext(), lstData));

                listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> adapter, View view, int position, long arg) {
                        Items selectedItem = (Items) listView.getAdapter().getItem(position);
                        loadLink(selectedItem);
                    }
                });

            }catch (Exception e){
                Log.e( "Exception", "ERROR: " + e.getMessage() );
            }
        }


    }
}