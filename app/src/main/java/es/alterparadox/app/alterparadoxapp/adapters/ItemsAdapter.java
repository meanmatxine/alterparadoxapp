package es.alterparadox.app.alterparadoxapp.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

import es.alterparadox.app.alterparadoxapp.R;
import es.alterparadox.app.alterparadoxapp.models.Items;

/**
 * Created by hetta on 06/02/2018.
 */

public class ItemsAdapter extends BaseAdapter {
    private Context context;
    private List<Items> items;
    private LayoutInflater inflater;
    public ItemsAdapter(Context context, List<Items> items) {
        this.context = context;
        this.items = items;
        this.inflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return this.items.size();
    }

    @Override
    public Object getItem(int position) {
        return this.items.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View rowView = convertView;

        if (convertView == null) {
            // Create a new view into the list.
            inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            rowView = inflater.inflate(R.layout.line, parent, false);
        }

        // Set data into the view.
        TextView txtName = (TextView) rowView.findViewById(R.id.textView);
        TextView txtPassword = (TextView) rowView.findViewById(R.id.textView2);


        Items item = this.items.get(position);

        txtName.setText(item.getName());
        txtPassword.setText(item.getDate());

        if(item.getFamily().equals("links")){
            txtPassword.setVisibility(View.GONE);
        }

        return rowView;


    }
}
