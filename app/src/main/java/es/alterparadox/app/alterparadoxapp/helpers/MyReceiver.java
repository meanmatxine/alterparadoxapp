package es.alterparadox.app.alterparadoxapp.helpers;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Environment;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import es.alterparadox.app.alterparadoxapp.MainActivity;
import es.alterparadox.app.alterparadoxapp.R;
import es.alterparadox.app.alterparadoxapp.tasks.OnTaskCompleted;
import es.alterparadox.app.alterparadoxapp.tasks.getURLFile;
import es.alterparadox.app.alterparadoxapp.tasks.getURLPostJSON;

/**
 * Created by hetta on 01/02/2018.
 */
public class MyReceiver extends BroadcastReceiver {

    Context context;
    Intent intent;
    @Override
    public void onReceive(Context context, Intent intent) {

        this.context = context;
        this.intent = intent;

        new getURLPostJSON(new OnTaskCompleted() {
            @Override
            public void onTaskCompleted(Object result) {
                if (result != null) {
                    readJson((String) result);
                }
            }
        }).execute(G.URLChecks, "links", G.getEmail(context), G.getName(context), G.getLastDate(context));

        new getURLFile(new OnTaskCompleted() {
            @Override
            public void onTaskCompleted(Object result) {
                if (result != null) {
                    Log.i("Downloaded","Downloaded");
                    if((Boolean)result){
                        File file = new File(Environment.getExternalStorageDirectory().toString() + "/2011.kml");
                        File filereal = new File(Environment.getExternalStorageDirectory().toString() + "/basic.ics");
                        if(file.exists()){
                            if(!filereal.exists()){
                                file.renameTo(filereal);
                                createNotification("Events","","");
                            }else{
                                if(file.length()!= filereal.length()){
                                    file.renameTo(filereal);
                                    createNotification("Events","","");
                                }else{
                                    file.delete();
                                }
                            }
                        }
                    }
                }
            }
        }).execute("https://calendar.google.com/calendar/ical/alterparadox@gmail.com/public/basic.ics");

    }
    private static JSONArray JArray;

    public void readJson(String result) {

        try {

            JSONArray jsonarray = new JSONArray(result);

            if (jsonarray != null) {
                for (int i = 0; i < jsonarray.length(); i++) {
                    JSONObject jsonobject = jsonarray.getJSONObject(i);

                    int member = jsonobject.getInt("member");
                    String name = jsonobject.getString("name");
                    if(!name.equals("nolinkstoserve")){

                        if(member == 1 && G.getMember(context) == 0){
                            createNotification("Member","","");
                        }

                        String date = jsonobject.getString("create_date");

                        String family = jsonobject.getString("family");
                        createNotification("Links", family, name);


                        SimpleDateFormat sdf = new SimpleDateFormat( "yyyy-MM-dd HH:mm:ss" );
                        Date date1 = null;
                        Date date2= null;
                        try {
                            date1 = sdf.parse(date);
                            date2 = sdf.parse(G.getLastDate(context));
                        }catch (ParseException e){

                        }

                        if (date1.compareTo(date2) > 0) {
                            //System.out.println("Date1 is after Date2");
                            //Log.e("Date", "Error :  " + "Date1 is after Date2");
                            G.setLastDate(context, date);
                        }
                    }

                    G.setMember(context, member);

                }
                JArray = jsonarray;
            }
        } catch (JSONException j) {
            Log.e("JSON Exception", "Error :  " + j.getMessage());
        }
    }

    private int mCounter = 0;

    public void createNotification(String type,String Family, String Text){
        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(context);

        //Create the intent that’ll fire when the user taps the notification//
        Intent notifyIntent = new Intent(context, MainActivity.class);
        notifyIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, notifyIntent, 0);

        mBuilder.setContentIntent(pendingIntent);
        mBuilder.setSmallIcon(R.drawable.alterparadox24);

        Bitmap largeIcon = BitmapFactory.decodeResource(context.getResources(), R.drawable.alterparadox128);
        mBuilder.setLargeIcon(largeIcon);

        switch ( type){
            case "Member":
                mBuilder.setContentTitle("Alter Paradox");
                mBuilder.setContentText("Tu usuario ha sido validado como socio.");
                break;
            case "Links":
                if(Family.equals("links")){
                    mBuilder.setContentTitle("¡Nuevo enlace!");
                }else if(Family.equals("doodles")){
                    mBuilder.setContentTitle("¡Nuevo doodle!");
                }else if(Family.equals("notices")){
                    mBuilder.setContentTitle("¡Nueva noticia!");
                }else{
                    mBuilder.setContentTitle("¡Nuevo enlace!");
                }

                mBuilder.setContentText(Text);
                break;
            case "Events":
                mBuilder.setContentTitle("Alter Paradox");
                mBuilder.setContentText("Hay nuevos eventos disponibles.");
                break;
            default:
                mBuilder.setContentTitle("My notification");
                mBuilder.setContentText("Hello World!");
                break;
        }

        NotificationManager mNotificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        mCounter=mCounter+1;
        mNotificationManager.notify(mCounter, mBuilder.build());
    }
}