package es.alterparadox.app.alterparadoxapp.tasks;

import android.content.ContentValues;
import android.os.Environment;
import android.util.Log;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.Map;

/**
 * Created by hetta on 04/02/2018.
 */

public final class getURLFile  extends DebuggableAsyncTask<String, Void, Boolean> {
    private final OnTaskCompleted listener;

    public getURLFile(OnTaskCompleted listener) {
        this.listener = listener;
    }

    @Override
    protected final Boolean doBackgroundTask(String... params) {

        URL url;
        int count;

        try {
            url = new URL(params[0]);
            URLConnection conection = url.openConnection();
            conection.connect();

            // this will be useful so that you can show a tipical 0-100%
            // progress bar
            int lenghtOfFile = conection.getContentLength();

            // download the file
            InputStream input = new BufferedInputStream(url.openStream(), 8192);

            // Output stream
            OutputStream output = new FileOutputStream(Environment.getExternalStorageDirectory().toString() + "/2011.kml");

            byte data[] = new byte[1024];

            long total = 0;

            while ((count = input.read(data)) != -1) {
                total += count;
                // publishing the progress....
                // After this onProgressUpdate will be called
                //publishProgress("" + (int) ((total * 100) / lenghtOfFile));

                // writing data to file
                output.write(data, 0, count);
            }

            // flushing output
            output.flush();

            // closing streams
            output.close();
            input.close();

            return true;
        } catch (Exception e) {
            Log.e("Error: ", e.getMessage());
        }

        return false;
    }

    @Override
    protected final void onPostExecute(Boolean requests) {
        Log.e("Response", "" + requests);
        listener.onTaskCompleted(requests);
    }
}
