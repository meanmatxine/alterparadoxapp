package es.alterparadox.app.alterparadoxapp.tasks;

import android.content.ContentValues;
import android.util.Log;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.Map;

/**
 * Created by hetta on 03/02/2018.
 */

public final class getURLPostJSON extends DebuggableAsyncTask<String, Void, String> {
    private final OnTaskCompleted listener;

    public getURLPostJSON(OnTaskCompleted listener) {
        this.listener = listener;
    }

    String server_response;

    @Override
    protected final String doBackgroundTask(String... params) {

        URL url;
        HttpURLConnection urlConnection = null;
        String email;
        String name;
        String lastdate;

        try {
            url = new URL(params[0]);
            String type = params[1];
            email = params[2];
            name = params[3];
            lastdate = params[4];

            urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.setRequestMethod("POST");
            urlConnection.setDoInput(true);
            urlConnection.setDoOutput(true);

            ContentValues values = new ContentValues();
            values.put("email", email);
            values.put("pass", "ThisIsAlterParadox");
            values.put("name", name);
            values.put("type", type);
            values.put("lastdate", lastdate);
            Log.i("Task",email+name+lastdate+url.toString());

            OutputStream os = urlConnection.getOutputStream();
            BufferedWriter writer = new BufferedWriter(
                    new OutputStreamWriter(os, "UTF-8"));
            writer.write(getQuery(values));
            writer.flush();
            writer.close();
            os.close();

            urlConnection.connect();

            int responseCode = urlConnection.getResponseCode();

            if(responseCode == HttpURLConnection.HTTP_OK){
                server_response = readStream(urlConnection.getInputStream());
                return server_response;
            }
            return null;
        } catch (MalformedURLException e) {
            e.printStackTrace();
            return null;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    private String getQuery(ContentValues values) throws UnsupportedEncodingException {
        StringBuilder result = new StringBuilder();
        boolean first = true;

        for (Map.Entry<String, Object> entry : values.valueSet())
        {
            if (first)
                first = false;
            else
                result.append("&");

            result.append(URLEncoder.encode(entry.getKey(), "UTF-8"));
            result.append("=");
            result.append(URLEncoder.encode(String.valueOf(entry.getValue()), "UTF-8"));
        }

        //Log.i("Result",result.toString() +" "+ String.valueOf(result));

        return result.toString();
    }

    public static String readStream(InputStream in) {
        BufferedReader reader = null;
        StringBuffer response = new StringBuffer();
        try {
            reader = new BufferedReader(new InputStreamReader(in));
            String line = "";
            while ((line = reader.readLine()) != null) {
                response.append(line);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return response.toString();
    }

    @Override
    protected final void onPostExecute(String requests) {
        Log.e("Response", "" + requests);
        listener.onTaskCompleted(requests);
    }
}
