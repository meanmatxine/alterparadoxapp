package es.alterparadox.app.alterparadoxapp.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

import es.alterparadox.app.alterparadoxapp.R;
import es.alterparadox.app.alterparadoxapp.models.Events;
import es.alterparadox.app.alterparadoxapp.models.Items;

/**
 * Created by hetta on 06/02/2018.
 */

public class EventsAdapter extends BaseAdapter {
    private Context context;
    private List<Events> events;

    public EventsAdapter(Context context, List<Events> events) {
        this.context = context;
        this.events = events;
    }

    @Override
    public int getCount() {
        return this.events.size();
    }

    @Override
    public Object getItem(int position) {
        return this.events.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View rowView = convertView;

        if (convertView == null) {
            // Create a new view into the list.
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            rowView = inflater.inflate(R.layout.calendarline, parent, false);
        }

        // Set data into the view.
        TextView txtName = (TextView) rowView.findViewById(R.id.txtName);
        TextView txtDate = (TextView) rowView.findViewById(R.id.txtDate);

        Events item = this.events.get(position);

        txtName.setText(item.getName());
        if(item.getLocation().equals("")){
            txtDate.setText(item.getDateStart() );
        }else{
            txtDate.setText(item.getDateStart() + " - " +item.getLocation());
        }


        if(item.getType().equals("link") || item.getType().equals("data")){
            txtDate.setVisibility(View.GONE);
        }else{
            txtDate.setVisibility(View.VISIBLE);
        }

        return rowView;
    }
}
