package es.alterparadox.app.alterparadoxapp;

import android.Manifest;
import android.app.AlarmManager;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import android.content.Context;
import android.support.v7.app.AppCompatDelegate;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;

import org.json.JSONArray;
import java.util.Calendar;

import es.alterparadox.app.alterparadoxapp.helpers.G;
import es.alterparadox.app.alterparadoxapp.helpers.MyReceiver;

public class MainActivity extends AppCompatActivity  {
    private int RC_SIGN_IN=1;
    private static final int ALARM_REQUEST_CODE = 1;

    private static Context context ;

    private static final int MY_PERMISSIONS_ACCESS_NETWORK_STATE = 1;
    private static final int MY_PERMISSIONS_GET_ACCOUNTS = 2;
    private static final int MY_PERMISSIONS_WRITE_EXTERNAL_STORAGE = 3;
    private static final int MY_PERMISSIONS_INTERNET = 4;

    private Menu menu;

    private MenuItem MenuUserAccount;
    private MenuItem MenuSignIn;
    private MenuItem MenuSignOut;

    private void askPermissions(){
        boolean askfor = false;
        // Here, thisActivity is the current activity
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_NETWORK_STATE) != PackageManager.PERMISSION_GRANTED) {
            askfor = true;
            //ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_NETWORK_STATE},  MY_PERMISSIONS_ACCESS_NETWORK_STATE);
        }
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.GET_ACCOUNTS) != PackageManager.PERMISSION_GRANTED) {
            askfor = true;
            //ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.GET_ACCOUNTS},  MY_PERMISSIONS_GET_ACCOUNTS);
        }
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            askfor = true;
        }
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.INTERNET) != PackageManager.PERMISSION_GRANTED) {
            askfor = true;
        }
        if(askfor){
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.INTERNET,Manifest.permission.WRITE_EXTERNAL_STORAGE,Manifest.permission.GET_ACCOUNTS,Manifest.permission.ACCESS_NETWORK_STATE},  MY_PERMISSIONS_INTERNET);
        }

    }
    @Override
    protected void onStart(){
        super.onStart();

        askPermissions();

        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
        // Check for existing Google Sign In account, if the user is already signed in
        // the GoogleSignInAccount will be non-null.
        GoogleSignInAccount account = GoogleSignIn.getLastSignedInAccount(this);
        //Log.i("GoogleAccount",account.getDisplayName() + "-" + account.getEmail());
        G.updateUI(this, account);
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);

        context = this;
        G.init(context);
        //ALARM TO CHECK
        CreateAlarm(1);

        //GOOGLE
        // Configure sign-in to request the user's ID, email address, and basic
        // profile. ID and basic profile are included in DEFAULT_SIGN_IN.
        //After Build a GoogleSignInClient with the options specified by gso.

        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();

        G.mGoogleSignInClient = GoogleSignIn.getClient(this, gso);
        //MENU
        Toolbar appbar = (Toolbar)findViewById(R.id.appbar);
        setSupportActionBar(appbar);

        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_action_menu);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        final DrawerLayout drawerLayout = (DrawerLayout)findViewById(R.id.drawer_layout);
        NavigationView navView = (NavigationView)findViewById(R.id.navview);
        navView.setItemIconTintList(null);

        Fragment fragment = new Fragment1();
        Bundle args = new Bundle();
        args.putString("load", "notice");
        fragment.setArguments(args);
        getSupportFragmentManager().beginTransaction().replace(R.id.content_frame, fragment).commit();

        MenuItem i = navView.getMenu().findItem(R.id.menu_seccion_1);
        i.setChecked(true);
        getSupportActionBar().setTitle(i.getTitle());

        navView.setNavigationItemSelectedListener(
                new NavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(MenuItem menuItem) {

                        boolean fragmentTransaction = false;
                        Fragment fragment = null;
                        Bundle args = new Bundle();

                        switch (menuItem.getItemId()) {
                            case R.id.menu_seccion_1:
                                fragment = new Fragment1();
                                args.putString("load", "notice");
                                fragmentTransaction = true;
                                break;
                            case R.id.menu_seccion_2:
                                fragment = new Fragment1();
                                args.putString("load", "links");
                                fragmentTransaction = true;
                                break;
                            case R.id.menu_seccion_3:
                                fragment = new Fragment1();
                                args.putString("load", "doodle");
                                fragmentTransaction = true;
                                break;
                            case R.id.menu_seccion_4:
                                fragment = new Fragment1();
                                args.putString("load", "calendar");
                                fragmentTransaction = true;
                                break;
                            case R.id.menu_opcion_1: //SIGN IN
                                Log.i("NavigationView", "Pulsada opción 1");
                                signIn();
                                break;
                            case R.id.menu_opcion_2: //SIGN OUT
                                Log.i("NavigationView", "Pulsada opción 2");
                                signOut();
                                break;
                        }

                        if(fragmentTransaction) {
                            fragment.setArguments(args);
                            getSupportFragmentManager().beginTransaction()
                                    .replace(R.id.content_frame, fragment)
                                    .commit();

                            menuItem.setChecked(true);
                            getSupportActionBar().setTitle(menuItem.getTitle());
                        }

                        drawerLayout.closeDrawers();

                        return true;
                    }
                });

        MenuUserAccount = navView.getMenu().findItem(R.id.menu_opcion_0);
        MenuSignIn = navView.getMenu().findItem(R.id.menu_opcion_1);
        MenuSignOut = navView.getMenu().findItem(R.id.menu_opcion_2);
        AccountStatus();
    }


    private void CreateAlarm(int when){

        AlarmManager manager = (AlarmManager) this.getSystemService(Context.ALARM_SERVICE);
        Intent intent  = new Intent(this, MyReceiver.class);
        PendingIntent pIntent = PendingIntent.getBroadcast(this, ALARM_REQUEST_CODE, intent,  PendingIntent.FLAG_CANCEL_CURRENT);
        //manager.set(AlarmManager.RTC_WAKEUP, System.currentTimeMillis() + when * 1000, pIntent);

        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.SECOND, 10);
        manager.setRepeating(AlarmManager.RTC_WAKEUP, cal.getTimeInMillis(),AlarmManager.INTERVAL_HOUR, pIntent);
        //manager.setRepeating(AlarmManager.RTC_WAKEUP, System.currentTimeMillis() ,1, pIntent);
        //manager.setInexactRepeating(AlarmManager.ELAPSED_REALTIME_WAKEUP, SystemClock.elapsedRealtime() + AlarmManager.INTERVAL_HOUR, AlarmManager.INTERVAL_HOUR, pIntent);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // Result returned from launching the Intent from GoogleSignInClient.getSignInIntent(...);
        if (requestCode == RC_SIGN_IN) {
            // The Task returned from this call is always completed, no need to attach
            // a listener.
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            handleSignInResult(task);

            //GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            //int statusCode = result.getStatus().getStatusCode();
        }
    }
    private void handleSignInResult(Task<GoogleSignInAccount> completedTask) {
        try {
            GoogleSignInAccount account = completedTask.getResult(ApiException.class);

            // Signed in successfully, show authenticated UI.
            G.updateUI(context, account);
            AccountStatus();
            //return;
        } catch (ApiException e) {
            // The ApiException status code indicates the detailed failure reason.
            // Please refer to the GoogleSignInStatusCodes class reference for more information.
            Log.w("GOOGLESIGN", "signInResult:failed code=" + e.getStatusCode() + " " + e.getStatusMessage());
            G.updateUI(context,null);
            AccountStatus();
        }
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        //MenuInflater inflater = getMenuInflater();
        //inflater.inflate(R.menu.optionmenu, menu);
        //this.menu = menu;
        AccountStatus();
        return true;
    }

    @Override
    protected void onResume() {
        super.onResume();
        AccountStatus();
    }

    @Override
    protected void onPause() {
        super.onPause();
        //unregisterReceiver(receiver);
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        final DrawerLayout drawerLayout = (DrawerLayout)findViewById(R.id.drawer_layout);

        switch (item.getItemId()) {

            case android.R.id.home:
                drawerLayout.openDrawer(GravityCompat.START);
                return true;
            default:
                // If we got here, the user's action was not recognized.
                // Invoke the superclass to handle it.
                Log.i("Menu","");

                return super.onOptionsItemSelected(item);
        }
    }

    private void AccountStatus(){
        if(this.menu !=null){
        }
        if(!G.Email.equals("")) {
            MenuUserAccount.setIcon(getResources().getDrawable(R.drawable.status_online));
            MenuUserAccount.setTitle(G.Name);
            MenuSignIn.setVisible(false);
            MenuSignOut.setVisible(true);
        }else{
            MenuUserAccount.setIcon(getResources().getDrawable(R.drawable.status_offline));
            MenuUserAccount.setTitle("Usuario desconocido");
            MenuSignIn.setVisible(true);
            MenuSignOut.setVisible(false);
        }
    }

    public void sendNotification(View view){

        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(context);

        //Create the intent that’ll fire when the user taps the notification//
        Intent notifyIntent = new Intent(context, MainActivity.class);
        notifyIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, notifyIntent, 0);

        mBuilder.setContentIntent(pendingIntent);
        mBuilder.setSmallIcon(R.drawable.alterparadox24);

        Bitmap largeIcon = BitmapFactory.decodeResource(context.getResources(), R.drawable.alterparadox128);
        mBuilder.setLargeIcon(largeIcon);
        String type = "Member";
        switch ( type){
            case "Member":
                mBuilder.setContentTitle("Alter Paradox");
                mBuilder.setContentText("Tu usuario ha sido validado como socio.");
                break;
            case "Link":
                mBuilder.setContentTitle("Alter Paradox");
                mBuilder.setContentText("Hay nuevos enlaces disponibles.");
                break;
            default:
                mBuilder.setContentTitle("My notification");
                mBuilder.setContentText("Hello World!");
                break;
        }

        NotificationManager mNotificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

        mNotificationManager.notify(001, mBuilder.build());
    }

    public void signIn(){
        Intent signInIntent = G.mGoogleSignInClient.getSignInIntent();
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    public void signOut(){

        G.mGoogleSignInClient.signOut()
                .addOnCompleteListener(this, new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        // ...
                        G.updateUI(context, null);
                        AccountStatus();
                    }
                });
    }

}
