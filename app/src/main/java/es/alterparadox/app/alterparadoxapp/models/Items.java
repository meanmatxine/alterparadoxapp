package es.alterparadox.app.alterparadoxapp.models;

import java.io.Serializable;

/**
 * Created by hetta on 06/02/2018.
 */

public class Items implements Serializable {
    private String Family;
    private String Name;
    private String Type;
    private String Date;
    private String Link;
    private int Member;

    public Items(String Family, String Name, String Type, String Date, String Link, int Member) {
        this.Family=Family;
        this.Name=Name;
        this.Type=Type;
        this.Date=Date;
        this.Link=Link;
        this.Member=Member;
    }
    public String getName() {
        return this.Name;
    }
    public void setName(String Value) {
        this.Name=Name;
    }
    public String getFamily() {
        return this.Family;
    }
    public void setFamily(String Family) {
        this.Family=Family;
    }
    public String getType() {
        return this.Type;
    }
    public void setType(String Type) {
        this.Type=Type;
    }
    public String getDate() {
        return this.Date;
    }
    public void setDate(String Date) {
        this.Date=Date;
    }
    public String getLink() {
        return this.Link;
    }
    public void setLink(String Date) {
        this.Link=Link;
    }
    public int getMember() {
        return this.Member;
    }
    public void setMember(int Calendar) {
        this.Member=Member;
    }

}
